/*
 * Copyright (C) 2017  Carmen Bianca Bakker <carmen@carmenbianca.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NODE_H
#define NODE_H

#include <vector>

#include "state.h"

struct Node {
    Node(const State &state) : state(state), parent() {};  // root node
    Node(const State &state, Node *parent) : state(state), parent(parent) {};  // all other nodes
    std::vector<State> cached_states;
    std::vector<Node*> children;
    bool terminal = false;
    const State state;
    Node *parent;
    int visits = 0;
    int wins = 0;
    ~Node() { for (Node *node : children) { delete node; } };
};

#endif
