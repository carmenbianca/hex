/*
 * Copyright (C) 2017  Carmen Bianca Bakker <carmen@carmenbianca.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MCTS_H
#define MCTS_H

#include <chrono>

#include "state.h"
#include "node.h"

class MCTS {
    public:
        MCTS(const State &state) : root(Node(state)) {};
        Move best_move(std::chrono::milliseconds delta);
    private:
        Node root;
        Node* ucb1(Node &node);
        Node* select_and_expand();
        Player do_simulation(const State &state);
        void back_propogate(Node *selected, Player winner);
        void do_round();
};

#endif
