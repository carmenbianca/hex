/*
 * Copyright (C) 2017  Carmen Bianca Bakker <carmen@carmenbianca.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <future>
#include <iostream>

#include "mcts.h"

// Perform a lot of MCTS round for delta amount of time.  The select the branch
// that has the highest percentage of cumulative wins.
Move MCTS::best_move(std::chrono::milliseconds delta) {
    using std::chrono::system_clock;

    system_clock::time_point start = system_clock::now();
    system_clock::time_point end = start + delta;

    // Only do rounds for a certain amount of time.
    while (system_clock::now() < end) {
        do_round();
    }
    std::cout << root.visits << std::endl;

    float best_ratio = -1;
    Node *best_node = &root;

    // Select branch with highest percentage
    for (Node *child : root.children) {
        float child_ratio = (float)child->wins / (float)child->visits;
        if (child_ratio > best_ratio) {
            best_ratio = child_ratio;
            best_node = child;
        }
    }

    // Figure out what the move to get to that node actually was...
    const std::vector<Move> legal_moves = root.state.getMoves();
    for (Move move : legal_moves) {
        if (root.state.doMoveFunc(move) == best_node->state) {
            return move;
        }
    }
    throw std::runtime_error("MCTS::best_move: Something went horribly wrong.");
}

// Select a node and expand it onto the tree.
Node* MCTS::select_and_expand() {
    Node *selected = &root;

    // Run forever (until game over)
    loop:
    while (!selected->terminal) {
        if (selected->cached_states.size() == 0) {
            const std::vector<Move> legal_moves = selected->state.getRelevantMoves();
            for (Move move : legal_moves) {
                selected->cached_states.push_back(selected->state.doMoveFunc(move));
            }
            if (legal_moves.size() == 0) {
                selected->terminal = true;
            }
        }
        if (selected->terminal) {
            break;
        }

        // SELECTION

        // Enough data is available to perform selection through UCB1
        if (selected->children.size() == selected->cached_states.size()) {
            selected = ucb1(*selected);
            continue;
        }
        // Not enough data is available; do a random move
        auto it = selected->cached_states.begin();
        std::advance(it, rand() % selected->cached_states.size());
        State random_state = *it;
        // Is this child state already in a child node?  If yes, go back to
        // selection.
        for (Node *child : selected->children) {
            if (random_state == child->state) {
                selected = child;
                goto loop;  // continue in outer loop
            }
        }

        // EXPANSION

        Node *new_node = new Node(random_state, selected);
        selected->children.push_back(new_node);
        return new_node;
    }
    return selected;
}

// Run a full Monte Carlo simulation until completion on the given state and
// return the winner.
Player MCTS::do_simulation(const State &state) {
    // Copy the state and perform a game on it.
    State result = State(state);
    std::vector<Move> moves = result.getMoves();
    std::random_shuffle(moves.begin(), moves.end());
    for (Move &move : moves) {
        result.doMove(move);
    }
    return result.getWinner();
}

// Go up the node tree to propogate the results.
void MCTS::back_propogate(Node *selected, Player winner) {
    Node *node = selected;

    while (true) {
        node->visits++;
        if (winner == node->state.getOpponent()) {
            node->wins++;
        }

        if (node->parent == NULL) {
            break;
        }

        node = node->parent;
    }
}

// Run a single Monte Carlo Tree Search round to completion on a node in the
// tree.
//
// The state that is selected for the simulation is determined either randomly,
// or through the UCB1 algorithm if there is enough data in the tree.
//
// An expansion of the tree occurs once at maximum per call of this function.
//
// The more often this function is called, the more representative the tree
// becomes of good and bad moves.
//
// It runs eight simulations simultaneously.
void MCTS::do_round() {
    // SELECTION AND EXPANSION
    Node *selected = select_and_expand();

    std::vector<std::future<Player>> results;

    // SIMULATION
    for (int i = 0; i < 8; i++) {
        results.push_back(std::async(std::launch::async, &MCTS::do_simulation,
                                     this, selected->state));
    }

    // BACKPROPOGATION
    for (unsigned i = 0; i < results.size(); i++) {
        back_propogate(selected, results[i].get());
    }
}

Node* MCTS::ucb1(Node &node) {
    float max = -1;
    Node *best_node = &node;
    for (Node *child : node.children) {
        float exploit = (float)child->wins / (float)child->visits;
        float explore = sqrt((2 * log(node.visits)) / (float)child->visits);
        float score = exploit + 1.4 * explore;
        if (score > max) {
            max = score;
            best_node = child;
        }
    }
    return best_node;
}
